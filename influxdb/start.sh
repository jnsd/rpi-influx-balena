#!/bin/bash

# steps copied and adjusted from: https://github.com/tutumcloud/influxdb/tree/master/1.0

set -m
INFLUX_HOST "localhost"
INFLUX_API_PORT = "8086"
# INFLUX_ADMIN_USR = "admin"
# INFLUX_ADMIN_PWD = "admin"

# setup and start influxdb: https://www.circuits.dk/install-grafana-influxdb-raspberry/
nohup influxd -config /etc/influxdb/influxdb.conf &

# wait for the startup of influxdb
RET=1
while [[ RET -ne 0 ]]; do
    sleep 3
    curl -k http://${INFLUX_HOST}:${INFLUX_API_PORT}/ping 2> /dev/null
    RET=$?
done

# create admin user and database
# influx -host=${INFLUX_HOST} -port=${INFLUX_API_PORT} -execute="CREATE USER ${INFLUX_ADMIN_USR} WITH PASSWORD '${INFLUX_ADMIN_PWD}' WITH ALL PRIVILEGES"
# influx -host=${INFLUX_HOST} -port=${INFLUX_API_PORT} -username=${INFLUX_ADMIN_USR} -password="${INFLUX_ADMIN_PWD}" -execute="create database \"simple_sensor\""
# influx -host=${INFLUX_HOST} -port=${INFLUX_API_PORT} -username=${INFLUX_ADMIN_USR} -password="${INFLUX_ADMIN_PWD}" -execute="grant all PRIVILEGES on simple_sensor to ${INFLUX_ADMIN_USR}"
influx -execute="create database simple_sensor"

# install and setup chronograf
#wget -P https://dl.influxdata.com/chronograf/releases/chronograf-1.7.2_linux_armhf.tar.gz
#tar xvfz /usr/local/chronograf-1.7.2_linux_armhf.tar.gz -C /usr/local
#rm -f chronograf-1.7.2_linux_armhf.tar.gz

# place current job in the foreground (as process)
tail -f /dev/null
