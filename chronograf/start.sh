#!/bin/bash

# install and setup chronograf
wget -P https://dl.influxdata.com/chronograf/releases/chronograf-1.7.2_linux_armhf.tar.gz
tar xvfz /usr/local/chronograf-1.7.2_linux_armhf.tar.gz -C /usr/local
rm -f chronograf-1.7.2_linux_armhf.tar.gz

# place current job in the foreground (as process)
tail -f /dev/null
