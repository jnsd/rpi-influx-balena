# RPi InfluxDB

Running InfluxDB on RPi with Balena OS (formerly Resin) enabling docker containers to run applications on the RPi.



## Option 1: Device Connected to Balena Account and Dashboard

### Get Balena OS Running on RPi

1. Make sure `python` points to version 2.7
2. Install balena-cli: `npm install balena-cli -g --production --unsafe-perm` ([source](https://github.com/balena-io/balena-cli))
3. Open the [Balena dashboard](https://dashboard.balena-cloud.com) and create an account
4. Go to preferences and add your public ssh key, i.e. content of  `~/.ssh/id_rsa.pub`
5. Create an app and device - make sure to activate wlan and add your wlan's ssid/password
6. Download the image
7. Install using Etcher
8. Start the device
9. ssh into the device's host OS (not user app - doesn't exist yet):
   `balena ssh --host` > select your device > type `balena info` to get some system data

### Create App

Create first app through Balena dashboard: [Get started with RPi and Balena](https://www.balena.io/docs/learn/getting-started/raspberrypi3/nodejs/)

1. Create sample app or your own: `git clone https://github.com/balena-io-projects/simple-server-node.git`

2. Open project folder and add git remote endpoint for balena: `git.balena-cloud.com` (found in the upper right corner in dashboard > devices) - **DO NOT** use git endpoint `git.www.balena.io` given in the above mentioned Get Started guide

3. Push the code to the balena git endpoint: `git push balena master`. The server will build the image (see dashboard > releases), upload it to the device (see dashboard > devices > updating) and start a container on the device. The first device upload takes a couple of minutes. All following uploads will be a lot faster.
4. ssh into your provisioned app: `balena ssh` > select your device
5. To push another git repo to the balena app git endpoint use `git push balena master -f`

Find balena base images here: https://www.balena.io/docs/reference/base-images/base-images/

### Git Workflow

1. Local development and testing
   1. Create new local branch `git checkout -b local_dev`
   2. Use this branch to push to balena repo, i.e. balena will build image and publish to device:
      1. `git add .`
      2. `git commit -a -m "commit description"`
      3. `git push balena local_dev:master`
2. Reduce local dev commits to one: `git rebase -i <sha of original branch off point`
   - Use `p` for oldest commit and `f` for all others to be squashed
   - Find local branch's branch off point with `git merge-base master local_dev`
   - Advantage: creates clean and readable master history and reduces number of merge conflicts
3. **(?) test this:** Rebase squashed commit on master: `git pull --rebase origin master` to get updates and rebase local branch on top of origin/master
4. Update master branch to point to latest commit
   1. Checkout master `git checkout master`
   2. Fast forward master to latest commit: `git merge --ff local_dev`
5. Push master to origin repo: `git push origin master`



## Option 2: Device not Connected to Balena Cloud

### Get Balena OS Running on RPi

https://www.balena.io/os/docs/raspberry-pi/getting-started/ (includes creation of first application image being built and run as container on the RPi)

**Important**: Does not work with `docker-compose.yml` and multiple containers



## Install InfluxDB

- move influx database to `/data` folder to make it persistent after restart
- Expose required ports, i.e. 8086 and ???



## Sources

[Docker container](https://hub.docker.com/r/philhawthorne/docker-influxdb-grafana/) to run InfluxDB and Grafana with persistence

[Running Telegraf inside a docker container](https://www.jacobtomlinson.co.uk/monitoring/2016/06/23/running-telegraf-inside-a-container/)

[Installing TIC stack on an RPi](https://community.influxdata.com/t/installing-on-a-raspberry-pi/2159)

[Run daemon service in foreground](https://www.dennyzhang.com/process_foreground)

